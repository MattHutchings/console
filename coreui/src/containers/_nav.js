export default [
  'CSidebarNav',
  [
    [
      'CSidebarNavLink',
      {
        props: {
          name: 'Dashboard',
          to: '/dashboard',
          icon: 'cui-speedometer',
          badge: {
            color: 'primary',
            text: 'NEW'
          }
        }
      }
    ],
    [
      'CSidebarNavTitle',
      ['Theme']
    ],
    [
      'CSidebarNavLink',
      {
        props: {
          name: 'Colors',
          to: '/theme/colors',
          icon: 'cui-drop'
        }
      }
    ],
    [
      'CSidebarNavLink',
      {
        props: {
          name: 'Typography',
          to: '/theme/typography',
          icon: 'cui-pencil'
        }
      }
    ],
    [
      'CSidebarNavTitle',
      ['Components']
    ],
    [
      'CSidebarNavDropdown',
      {
        props: {
          name: 'Base',
          route: '/base',
          icon: 'cui-puzzle',
        }
      },
      [
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Breadcrumbs',
              to: '/base/breadcrumbs',
              icon: 'cui-puzzle'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Cards',
              to: '/base/cards',
              icon: 'cui-puzzle'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Carousels',
              to: '/base/carousels',
              icon: 'cui-puzzle'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Collapses',
              to: '/base/collapses',
              icon: 'cui-puzzle'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Jumbotrons',
              to: '/base/jumbotrons',
              icon: 'cui-puzzle'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'List Groups',
              to: '/base/list-groups',
              icon: 'cui-puzzle'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Navs',
              to: '/base/navs',
              icon: 'cui-puzzle'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Navbars',
              to: '/base/navbars',
              icon: 'cui-puzzle'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Paginations',
              to: '/base/paginations',
              icon: 'cui-puzzle'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Popovers',
              to: '/base/popovers',
              icon: 'cui-puzzle'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Progress Bars',
              to: '/base/progress-bars',
              icon: 'cui-puzzle'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Switches',
              to: '/base/switches',
              icon: 'cui-puzzle'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Tabs',
              to: '/base/tabs',
              icon: 'cui-puzzle'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Tooltips',
              to: '/base/tooltips',
              icon: 'cui-puzzle'
            }
          }
        ]
      ]
    ],
    [
      'CSidebarNavDropdown',
      {
        props: {
          name: 'Buttons',
          route: '/buttons',
          icon: 'cui-cursor',
        }
      },
      [
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Buttons',
              to: '/buttons/standard-buttons',
              icon: 'cui-cursor'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Button Dropdowns',
              to: '/buttons/dropdowns',
              icon: 'cui-cursor'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Button Groups',
              to: '/buttons/button-groups',
              icon: 'cui-cursor'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Brand Buttons',
              to: '/buttons/brand-buttons',
              icon: 'cui-cursor'
            }
          }
        ]
      ]
    ],
    [
      'CSidebarNavLink',
      {
        props: {
          name: 'Charts',
          to: '/charts',
          icon: 'cui-pie-chart'
        }
      }
    ],
    [
      'CSidebarNavDropdown',
      {
        props: {
          name: 'Editors',
          route: '/editors',
          icon: 'cui-code',
        }
      },
      [
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Code editors',
              to: '/editors/code-editors',
              icon: 'cui-code',
              badge: {
                color: 'danger',
                text: 'PRO'
              }
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Text editors',
              to: '/editors/text-editors',
              icon: 'cui-justify-left'
            }
          }
        ]
      ]
    ],
    [
      'CSidebarNavDropdown',
      {
        props: {
          name: 'Forms',
          route: '/forms',
          icon: { name: 'notes' },
        }
      },
      [
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Basic forms',
              to: '/forms/basic-forms',
              icon: { name: 'notes' },
            }
          }
        ],
        [
        'CSidebarNavLink',
          {
            props: {
              name: 'Adcanced forms',
              to: '/forms/advanced-forms',
              icon: { name: 'notes' },
              badge: {
                color: 'danger',
                text: 'PRO'
              }
            }
          }
        ],
        [
        'CSidebarNavLink',
          {
            props: {
              name: 'Validation forms',
              to: '/forms/validation-forms',
              icon: { name: 'notes' },
              badge: {
                color: 'danger',
                text: 'PRO'
              }
            }
          }
        ]
      ]
    ],
    [
      'CSidebarNavLink',
      {
        props: {
          name: 'Google Maps',
          to: '/google-maps',
          icon: 'cui-map',
          badge: {
            color: 'danger',
            text: 'PRO'
          }
        }
      }
    ],
    [
      'CSidebarNavDropdown',
      {
        props: {
          name: 'Icons',
          route: '/icons',
          icon: 'cui-star',
        }
      },
      [
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'CoreUI Icons',
              to: '/icons/coreui-icons',
              icon: 'cui-star',
              badge: {
                color: 'info',
                text: 'NEW'
              }
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Brands',
              to: '/icons/brands',
              icon: 'cui-star'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Flags',
              to: '/icons/flags',
              icon: 'cui-star'
            }
          }
        ]
      ]
    ],
    [
      'CSidebarNavDropdown',
      {
        props: {
          name: 'Notifications',
          route: '/notifications',
          icon: 'cui-bell',
        }
      },
      [
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Alerts',
              to: '/notifications/alerts',
              icon: 'cui-bell'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Badges',
              to: '/notifications/badges',
              icon: 'cui-bell'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Modals',
              to: '/notifications/modals',
              icon: 'cui-bell'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Toaster',
              to: '/notifications/toaster',
              icon: 'cui-bell',
              badge: {
                color: 'danger',
                text: 'PRO'
              }
            }
          }
        ]
      ]
    ],
    [
      'CSidebarNavDropdown',
      {
        props: {
          name: 'Plugins',
          route: '/plugins',
          icon: { name: 'input-power' },
        }
      },
      [
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Draggable',
              to: '/plugins/draggable',
              icon: { name: 'cursor-move' },
              badge: {
                color: 'danger',
                text: 'PRO'
              }
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Calendar',
              to: '/plugins/calendar',
              icon: 'cui-calendar',
              badge: {
                color: 'danger',
                text: 'PRO'
              }
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Spinners',
              to: '/plugins/spinners',
              icon: { name: 'circle' },
              badge: {
                color: 'danger',
                text: 'PRO'
              }
            }
          }
        ]
      ]
    ],
    [
      'CSidebarNavDropdown',
      {
        props: {
          name: 'Tables',
          route: '/tables',
          icon: 'cui-list',
        }
      },
      [
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Basic Tables',
              to: '/tables/tables',
              icon: 'cui-list',
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Advanced tables',
              to: '/tables/advanced-tables',
              icon: { name: 'list-rich' }
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Lazy loading tables',
              to: '/tables/lazy-loading-tables',
              icon: { name: 'list-rich' }
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Lazy loading tables scroll',
              to: '/tables/lazy-loading-tables-scroll',
              icon: { name: 'list-rich' }
            }
          }
        ]
      ]
    ],
    [
      'CSidebarNavLink',
      {
        props: {
          name: 'Widgets',
          to: '/widgets',
          icon: 'cui-calculator',
          badge: {
            color: 'primary',
            text: 'NEW',
            pill: true
          }
        }
      }
    ],
    [
      'CSidebarNavDivider',
    ],
    [
      'CSidebarNavTitle',
      ['Extras']
    ],
    [
      'CSidebarNavDropdown',
      {
        props: {
          name: 'Pages',
          route: '/pages',
          icon: 'cui-star',
        }
      },
      [
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Login',
              to: '/login',
              icon: 'cui-star'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Register',
              to: '/register',
              icon: 'cui-star'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Error 404',
              to: '/pages/404',
              icon: 'cui-star'
            }
          }
        ],
        [
          'CSidebarNavLink',
          {
            props: {
              name: 'Error 500',
              to: '/pages/500',
              icon: 'cui-star'
            }
          }
        ]
      ]
    ],
    [
      'CSidebarNavDropdown',
      {
        props: {
          name: 'Apps',
          route: '/apps',
          icon: 'cui-layers',
        }
      },
      [
        [
          'CSidebarNavDropdown',
          {
            props: {
              name: 'Invoicing',
              route: '/apps/invoicing',
              icon: { name: 'spreadsheet' }
            }
          },
          [
            [
              'CSidebarNavLink',
              {
                props: {
                  name: 'Invoice',
                  to: '/apps/invoicing/invoice',
                  icon: { name: 'spreadsheet' },
                  badge: {
                    color: 'danger',
                    text: 'PRO'
                  }
                }
              }
            ]
          ]
        ],
        [
          'CSidebarNavDropdown',
          {
            props: {
              name: 'Email',
              route: '/apps/email',
              icon: 'cui-envelope-closed'
            }
          },
          [
            [
              'CSidebarNavLink',
              {
                props: {
                  name: 'Inbox',
                  to: '/apps/email/inbox',
                  icon: 'cui-envelope-closed',
                  badge: {
                    color: 'danger',
                    text: 'PRO'
                  }
                }
              }
            ],
            [
              'CSidebarNavLink',
              {
                props: {
                  name: 'Message',
                  to: '/apps/email/message',
                  icon: 'cui-envelope-open',
                  badge: {
                    color: 'danger',
                    text: 'PRO'
                  }
                }
              }
            ],
            [
              'CSidebarNavLink',
              {
                props: {
                  name: 'Compose',
                  to: '/apps/email/compose',
                  icon: 'cui-envelope-letter',
                  badge: {
                    color: 'danger',
                    text: 'PRO'
                  }
                }
              }
            ]
          ]
        ]
      ]
    ],
    [ 'CSidebarNavDivider', { attrs: { class: 'm-2' } } ],
    [ 'CSidebarNavTitle', { props: { name: 'Labels' } } ],
    [ 
      'CSidebarNavLink', 
      { 
        props: { 
          name: 'Label danger',
          icon: 'cui-star text-danger',
          label: true
        } 
      } 
    ],
    [ 
      'CSidebarNavLink', 
      { 
        props: { 
          name: 'Label info',
          icon: 'cui-star text-info',
          label: true
        } 
      } 
    ],
    [ 
      'CSidebarNavLink', 
      { 
        props: { 
          name: 'Label warning',
          icon: 'cui-star text-warning',
          label: true
        } 
      } 
    ]
  ]
]

